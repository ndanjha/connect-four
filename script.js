let board = document.getElementById('board')
let player = 1
let winCounter = 1


for(i=0; i<7; i++){
let col = document.createElement('div')
col.classList.add = 'col'
col.id= 'col' + 1
board.appendChild(col)

for(j=0; j<6; j++){
    let row = document.createElement('div')
    row.id = `x${i}y${5 - j}`
    row.classList.add('cell')
    col.append(row)
}
}

let cells = document.querySelectorAll('.cell')
let cellsArray = Array(...cells)
cellsArray.forEach( cell => cell.addEventListener('click', dropChecker ))


function findOpenCell(colClicked){
  let  checkingHere = Array.from(colClicked.children).reverse()
for(cell of checkingHere){
    if( !(cell.classList.contains('player1') || cell.classList.contains('player2'))){
        return cell
    }
}
}

function checkForWin(fromChecker, direction){
let x = Number(fromChecker.id[1])
let y = Number(fromChecker.id[3])


    if(direction == 'left') nextChecker = document.querySelector(`#x${x - 1}y${y}`)
if (direction === 'right') nextChecker = document.querySelector(`#x${x + 1}y${y}`)
if (direction === 'down') nextChecker = document.querySelector(`#x${x}y${y - 1}`)
if (direction === 'dUpLeft') nextChecker = document.querySelector(`#x${x - 1}y${y + 1}`)
if (direction === 'dDownRight') nextChecker = document.querySelector(`#x${x + 1}y${y - 1}`)
if (direction === 'dUpRight') nextChecker = document.querySelector(`#x${x + 1}y${y + 1}`)
if (direction === 'dDownLeft') nextChecker = document.querySelector(`#x${x - 1}y${y - 1}`)


if(!nextChecker)void 0

else if(nextChecker.classList.contains(`player${player}`)){
  winCounter += 1
      checkForWin(nextChecker, direction)
}
}

function dropChecker(thing){
    let colClicked = thing.path[1]
    let lastDroppedChecker = findOpenCell(colClicked)
    lastDroppedChecker.classList.add(`player${player}`)
    
  checkForWin(lastDroppedChecker, 'left')
  checkForWin(lastDroppedChecker, 'right')
winCounter < 4 ? winCounter = 1: alert('winner')

checkForWin(lastDroppedChecker, 'down')
winCounter < 4 ? winCounter = 1: alert('winner')

checkForWin(lastDroppedChecker, 'dUpLeft')
checkForWin(lastDroppedChecker, 'dDownRight')
winCounter < 4 ? winCounter = 1: alert('winner')

checkForWin(lastDroppedChecker, 'dUpRight')
checkForWin(lastDroppedChecker, 'dDownLeft')
winCounter < 4 ? winCounter = 1: alert('winner')

if(player === 1) player += 1
else player -= 1




}